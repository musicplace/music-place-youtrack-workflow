/**
 * Copyright JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 const http = require('@jetbrains/youtrack-scripting-api/http');

 function makeBasicConnection(options) {
   const url = 'https://musicplace.youtrack.cloud/api';
   const token = 'perm:YW5lcmVtaW4=.NDktMg==.aYBVzcTZtNisKHUFjxoe0qtYNnOtNt';
   const connection = new http.Connection(url);
   connection.addHeader('Content-Type', 'application/json');
   connection.addHeader("Authorization", "Bearer " + token);
   return connection;
 }
 
 const processHttpRequestResult = (result, uri) => {
   if (!result.isSuccess) {
     if (result.exception) {
       throw result.exception;
     }
     console.log(JSON.stringify(Object.keys(result)));
     throw 'Failed to load data from ' + uri + ' code ' + result.code + ' error ' +  result.error;
   } else {
     if (isJson(result.response)) {
       return JSON.parse(result.response);
     } else {
       return result.response;
     }
   }
 };
 
 function isJson(str) {
     try {
         JSON.parse(str);
     } catch (e) {
         return false;
     }
     return true;
 }
 
 function get(uri, queryParams, options={}) {
   const result = makeBasicConnection(options).getSync(uri, queryParams);
   return processHttpRequestResult(result, uri);
 }
 
 function post(uri, payload, queryParams = null, options={}) {
   const result = makeBasicConnection(options).postSync(uri, queryParams, payload);
   return processHttpRequestResult(result, uri);
 }
 
 function put(uri, payload, queryParams = null, options={}) {
   const result = makeBasicConnection(options).putSync(uri, queryParams, payload);
   return processHttpRequestResult(result, uri);
 }
 
 function del(uri, payload, queryParams = null, options={}) {
   const result = makeBasicConnection(options).deleteSync(uri, queryParams);
   return processHttpRequestResult(result, uri);
 }
 
 module.exports = {
   get: get,
   post: post,
   put: put,
   delete: del
 };