/* eslint-disable */
var entities = require('@jetbrains/youtrack-scripting-api/entities');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');
var Connection = require('./qase-connection'); 
var YouTrackConnection = require('./youtrack-connection');
const {getParamNames, getArray, getMethods, Logger} = require('mp-utils/mp-utils');
      
exports.rule = entities.Issue.onSchedule({
  title: "add-test-case-on-schedule",
  cron: '0 * * ? * *',
  search: 'id задачи: MP-1',
    action: sync
});

// exports.rule = entities.Issue.onChange({
//   title: "add-test-case-on-schedule",
//   guard: sync,
//   action: function(ctx) {},
//   requirements: {}
// });

function sync(ctx) {
    const logger = new Logger(true);
	console.log("sync-case started");
    console.log(ctx.issue.id);
    let qase_project_prefix = "MP";
    // получаем все тест-кейсы из Qase
    let test_cases = Connection.get(`/case/${qase_project_prefix}?limit=90&offset=0`);
	
    let test_runs = Connection.get(`/run/${qase_project_prefix}?limit=90&offset=0`);
    console.log(test_runs)
	syncTestCases(test_cases)
    
    let qase_tags = [];
    for (let test_case of test_cases.result.entities) {
	  for (let tag of test_case.tags) {
        qase_tags.push(tag.title);
      }
    }

    let project = entities.Project.findByKey("MP");
	let features = []
    let feature_corrs = []
    let it = project.issues.values();
    let done = false;
    while (!done) {
  
      let val = it.next();
      done = val.done;
      let issue = val.value;
      if (!issue) { continue; }
      
      // если не фича, очищаем поле
      clearQaseIds(issue)
      if (issue.fields['Тип'].name == 'Feature') {
          features.push(issue)
      }
      // если таска/баг из Youtrack совпадает с тест-кейсом в Qase
      if (qase_tags.includes(issue.id)) {
		  const qase_feature_corrs = syncQaseIdsInIssue(test_cases, issue, project);
          for (let q of qase_feature_corrs) {
            if (!feature_corrs.find(f => f.id == q.id && f.feature_id == q.feature_id)) {
              feature_corrs.push(q)
            }
          }
          
      }
    }
    
    // получаем список всех Qase ID
    let builds = getBuilds(project);
      
    for (let feature of features) {
      const feature_corr = feature_corrs.filter(f => f.feature_id == feature.id)
      if (!feature_corr.length) { feature['Qase IDs'].clear(); continue; }
      
      // берем текущие значения в фиче
      let qase_ids_array = getArray(feature['Qase IDs'].values());
      let _youtrack_qase_ids = qase_ids_array.map(q => q.name);

      // берем все кейсы, которые связаны с таской issue и мапим в ИД
      let _qase_case_ = feature_corr // test_cases.result.entities.filter(t => t.tags.filter(tag => tag.title == issue.id).length > 0)
      let _qase_case_ids = _qase_case_.map(q => `MP-${q.id}`)
      
      let deleted_ids = _youtrack_qase_ids.filter(y => !_qase_case_ids.includes(y))
      let added_ids = _qase_case_ids.filter(y => !_youtrack_qase_ids.includes(y))

    //   console.log(feature.id, deleted_ids, added_ids)
      for (let id of deleted_ids) {
        feature['Qase IDs'].delete(qase_ids_array.find(q => q.name == id))
      }

       for (let id of added_ids) {
    //       console.log(builds.find(y => y.name == id))
          feature['Qase IDs'].add(builds.find(y => y.name == id))
       }

//        if (!deleted_ids.length && !added_ids.length) {
//          return []
//        }
      describe_feature(feature, feature_corr)
    }
    console.log("sync-case finished");
}

function syncTestCases(test_cases) {
    let youtrack_qase_ids_type_id = "133-2";
    // получаем ИД из Qase, которые сохранены в Youtrack
    let youtrack_qase_ids = YouTrackConnection.get(`/admin/customFieldSettings/bundles/build/${youtrack_qase_ids_type_id}/values?fields=$type,archived,assembleDate,avatarUrl,color(id),description,fullName,hasRunningJob,id,isResolved,issueRelatedGroup(icon),localizedName,login,name,ordinal,owner(id,login,ringId),releaseDate,released,ringId,showLocalizedNameInAdmin,teamForProject(ringId),usersCount`);
    
//     logger.log(youtrack_qase_ids);
	let test_cases_ids = test_cases.result.entities.map(t => `MP-${ t.id}`);
    let youtrack_cases_ids = youtrack_qase_ids.map(t => t.name);

    let added_ids = test_cases_ids.filter(t => !youtrack_cases_ids.includes(t));
    let removed_cases = youtrack_qase_ids.filter(t => !test_cases_ids.includes(t.name));    
//     console.log(removed_cases);
    
    // Добавляем в Youtrack недостающие ID тест-кейсов из Qase
    for (let id of added_ids) {
      let raw_data = {
        "assembleDate": null,
        "name": id,
        "$type": "BuildBundleElement"
      };
      try {
        YouTrackConnection.post(`/admin/customFieldSettings/bundles/build/${youtrack_qase_ids_type_id}/values`, raw_data, null);
      } catch (e) {
        console.log();
      }
    }
    
    // Удаляем из Youtrack лишние ID тест-кейсов
    for (let test_case of removed_cases) {
      YouTrackConnection.delete(`/admin/customFieldSettings/bundles/build/${youtrack_qase_ids_type_id}/values/${test_case.id}`);
    }
}

function getBuilds(project) {
  let qase_field = getArray(project.fields.values()).filter(a => a.name == "Qase IDs")
  let builds = getArray(qase_field.values())[0].values
  builds = getArray(builds.values())
//   console.log(builds)
  return builds;
}

function clearQaseIds(issue) {
  // если не фича, очищаем поле
  let ids = getArray(issue.fields['Qase IDs'].values());
  if (ids.length > 0 && issue.fields['Тип'].name != 'Feature') {
       issue.fields['Qase IDs'].clear()
  }
}

function syncQaseIdsInIssue(test_cases, issue, project) {
      
  //           console.log(issue.links['subtask of'], typeof issue.links['subtask of'], issue.id)   
  // по таске находим стори и затем фичу
  let story = getArray(issue.links['subtask of'].values())[0]
  let feature = getArray(story.links['subtask of'].values())[0]

  // берем все кейсы, которые связаны с таской issue и мапим в ИД
  let _qase_case_ = test_cases.result.entities.filter(t => t.tags.filter(tag => tag.title == issue.id).length > 0)

  return _qase_case_.map(q => ({
     id: q.id, title: q.title, feature_id: feature.id
   }))
}

function describe_feature(feature, feature_corr) {

     let description = ""
     let matches = feature.description ? 
         feature.description.match(new RegExp("(\| ID в Qase\| Описание \|[^]*?##### Таблица генерируется автоматически на основании данных с Qase)(\n[^]*)", 'd')) : null

     let current_qase_ids = getArray(feature['Qase IDs'].values());
     let str = "| ID в Qase| Описание |\n| --- | --- |\n";
     for (let current_qase of current_qase_ids) {
//        console.log(feature_corr, current_qase)
       const test_case = feature_corr.find(q => q.id == current_qase.name.slice(3))
       str += `| [${current_qase.name}](https://app.qase.io/project/MP?case=${test_case.id}&previewMode=modal&suite=1) | ${test_case.title} |\n`;
     }
     let end_str = "##### Таблица генерируется автоматически на основании данных с Qase"
     str += end_str;
      if (!matches) {
          description = str + "\n" + feature.description
      } else {
          description = str + (matches[2] ?? "")
      }  

    feature.description = description
}