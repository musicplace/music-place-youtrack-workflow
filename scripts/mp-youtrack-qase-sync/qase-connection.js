/**
 * Copyright JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 const http = require('@jetbrains/youtrack-scripting-api/http');

 function makeBasicConnection({is_app}) {
   const url = is_app ? 'https://app.qase.io' : 'https://api.qase.io' + '/v1';
 //   const login = 'liliasamig@gmail.com';
   const token = 'f9cb8ddd70fc8194b46d40843731f7b6f99dcc0cdb26e453d05c4d87e32ae1f8';
   const connection = new http.Connection(url);//.basicAuth(login, token);
   connection.addHeader('Content-Type', 'application/json');
   connection.addHeader('Token', token);
   return connection;
 }
 
 const processHttpRequestResult = (result, uri) => {
   if (!result.isSuccess) {
     if (result.exception) {
       throw result.exception;
     }
     throw 'Failed to load data from ' + uri + ' code ' + result.code;
   } else {
     return JSON.parse(result.response);
   }
 };
 
 function get(uri, queryParams, options={is_app: false}) {
   const result = makeBasicConnection(options).getSync(uri, queryParams);
   return processHttpRequestResult(result, uri);
 }
 
 function post(uri, payload, queryParams = null, options={is_app: false}) {
   const result = makeBasicConnection(options).postSync(uri, queryParams, payload);
   return processHttpRequestResult(result, uri);
 }
 
 function put(uri, payload, queryParams = null, options={is_app: false}) {
   const result = makeBasicConnection(options).putSync(uri, queryParams, payload);
   return processHttpRequestResult(result, uri);
 }
 
 module.exports = {
   get: get,
   post: post,
   put: put
 };