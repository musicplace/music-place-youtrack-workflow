/**
 * This is a template for a custom script. Here you can add any
 * functions and objects you want to. To use these functions and objects in other
 * scripts, add them to the 'exports' object.
 * Use the following syntax to reference this script:
 *
 * `var myScript = require('./this-script-name');`
 *
 * For details, read the Quick Start Guide:
 * https://www.jetbrains.com/help/youtrack/cloud/2022.3/Quick-Start-Guide-Workflows-JS.html
 */

// TODO: define functions and objects


var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
     result = [];
  return result;
}

const getArray = (it) => {
  
  let result = []
  let done = false;
  while (!done) {
    let val = it.next()
    if (val && val.value) {
      result.push(val.value)
    }
    done = val && val.done;
  }
  return result;
}

const getMethods = (obj) => {
  let properties = new Set()
  let currentObj = obj
  do {
    Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
  } while ((currentObj = Object.getPrototypeOf(currentObj)))
  return [...properties.keys()].filter(item => typeof obj[item] === 'function')
}

function Logger(useDebug = true) {
  return {
    log: (...args) => useDebug && console.log(...args),
    warn: (...args) => useDebug && console.warn(...args),
    error: (...args) => useDebug && console.error(...args)
  };
}

module.exports = {
  getParamNames, getArray, getMethods, Logger
};

