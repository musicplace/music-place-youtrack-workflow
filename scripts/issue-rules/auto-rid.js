/* eslint-disable */
var entities = require('@jetbrains/youtrack-scripting-api/entities');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');
var Connection = require('mp-youtrack-qase-sync/qase-connection'); 
var YouTrackConnection = require('mp-youtrack-qase-sync/youtrack-connection');
const {getParamNames, getArray, getMethods, Logger} = require('mp-utils/mp-utils');
      
// exports.rule = entities.Issue.onSchedule({
//   title: "fill-RID",
//   cron: '0 * * ? * *',
//   search: 'id задачи: MP-1',
//     action: fill
// });

exports.rule = entities.Issue.onChange({
  title: "fill-RID-on-change",
  guard: function(ctx) { return true; },
  action: fill,
  requirements: {}
});

function fill() {
    
    let project = entities.Project.findByKey("MP");

    let it = project.issues.values();
    let done = false;
    const epics = []
    while (!done) {
      let val = it.next();
      done = val.done;
      let issue = val.value;
      if (!issue) { continue; }
      
      if (issue.fields['Тип'].name == 'Epic') {
       epics.push(issue);
  	  } else if (issue.fields['Тип'].name == 'User Story') {
      }
      
//       issue.fields['RID'] = "43";
    }
    const epic_regexp = new RegExp('FR ([0-9]+)')
    let maxEpicIndex = getMaxNumber(epics, epic_regexp, 1)
    
    for (let epic of epics) {
      if (not_rid(epic, epic_regexp)) {
        maxEpicIndex += 1;
        epic.fields['RID'] = `FR ${maxEpicIndex}`;
      }
      
      let features = getArray(epic.links['parent for'].values())
      const feature_regexp = new RegExp('FR ([0-9]+).([0-9]+)')
      let maxFeatureIndex = getMaxNumber(epics, feature_regexp, 2)
    
      for (let feature of features) {
        if (not_rid(feature, feature_regexp)) {
          maxFeatureIndex += 1;
          feature.fields['RID'] = `${epic.fields['RID']}.${maxFeatureIndex}`;
        }
      }
    }
}

function getMaxNumber(array, regexp, index) {
  let numbers = array.map(e => not_rid(e, regexp) ? "0" : e.fields['RID'].match(regexp)[index])
  .map(e => Number(e))
  numbers = numbers.sort(function(a, b) {
      return -a + b;
  })
  return numbers[0]
}

function not_rid(e, regexp){ 
  return !e.fields['RID'] || !e.fields['RID'].match(regexp) || e.fields['RID'].match(regexp).length < 2
}
