/* eslint-disable */
var entities = require('@jetbrains/youtrack-scripting-api/entities');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');



exports.rule = entities.Issue.onChange({
  title: "confirm-req",
  guard: function(ctx) {
    const logger = new Logger(true);

    // --- #1 checkUpdatedBy ---
    const issue_0 = ctx.issue;
    const user_0 = ctx.currentUser;
    
    if (issue_0.fields["Тип"].name == "User Story" && (issue_0.isChanged("Estimation") || issue_0.isChanged("Приоритет") || issue_0.isChanged("Сложность"))) {
      return true;
    }
    return false;

  },
  action: function(ctx) {
    const logger = new Logger(true);


    // --- #1 changeIssueFieldValue ---
    const issue_1 = ctx.issue;

    const changeIssueFieldValueFn_0 = () => {
	  
      let prior = getPrior(issue_1.fields['Приоритет'].name)
      let difficulty = issue_1.fields['Сложность'] ? getDifficulty(issue_1.fields['Сложность'].name) : 1
      let estimate_time = getEstimationPoint(issue_1.fields["Estimation"])

      issue_1.fields['Оценка'] = (estimate_time * 1.3) / (difficulty / 3.0 < 1 ? 1 : difficulty / 3.0) + (prior / 3.0 < 1 ? 1 : prior / 3.0);
      issue_1.fields['Оценка'] = issue_1.fields['Оценка'] < 1 ? 1 : issue_1.fields['Оценка'];
      console.log(issue_1.fields['Оценка'])
    };

    changeIssueFieldValueFn_0();

  },
//   requirements: {
//     Enum_844611: {
//       name: "Утвержден",
//       type: entities.EnumField.fieldType,
//       n356: {name: "нет"}
//     }
//   }
});

function showMessage(what) {
  workflow.message(what, 'Не удалось обновить задачу.');
}

function getPrior(name) {
  let priors = {
    'Minor': 1,
    'Normal': 1,
    'Major': 2,
    'Critical': 3,
    'Show-stopper': 4,
  }
  return priors[name] ?? 1
}

function getDifficulty(name) {
  let difficulties = {
    'Просто': 1,
    'Непросто': 2,
    'Трудно': 3,
    'Нерешаемо': 10,
  }
  return difficulties[name] ?? 1
}

function getEstimationPoint(estimate) {
  if (estimate < 2) {
    return 1;
  } else if (estimate < 5) {
    return 2;
  } else if (estimate < 5) {
    return 3;
  } else if (estimate < 9) {
    return 4;
  } else if (estimate < 17) {
    return 5;
  }
  return 6;
}

function Logger(useDebug = true) {
  return {
    log: (...args) => useDebug && console.log(...args),
    warn: (...args) => useDebug && console.warn(...args),
    error: (...args) => useDebug && console.error(...args)
  };
}