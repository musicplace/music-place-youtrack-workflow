/* eslint-disable */
var entities = require('@jetbrains/youtrack-scripting-api/entities');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');



exports.rule = entities.Issue.onChange({
  title: "confirm-req",
  guard: function(ctx) {
    const logger = new Logger(true);

    // --- #1 checkUpdatedBy ---
    const issue_0 = ctx.issue;
    const user_0 = ctx.currentUser;
    
// 	logger.log(JSON.stringify(issue_0.isChanged("description")));
    const checkUpdatedByFn_0 = () => {
 	  console.log(issue_0.isChanged("description") , ((issue_0.updatedBy || {}).login === (user_0 || {}).login));
      // если описание изменилось, значит изменились требования
      return issue_0.isChanged("description") && ["Epic", "Feature"].includes(issue_0.fields["Тип"])//&& ((issue_0.updatedBy || {}).login === (user_0 || {}).login);
    };

    // --- #2 notOperator ---

    // --- #3 User.isInGroup ---
    logger.log("Running scripts for the \"Пользователь состоит в группе\" block");
    const user_1 = ctx.currentUser;
    const string_0 = `Key User Group`;

    const UserisInGroupFn_0 = () => {
      if (user_1 === null || user_1 === undefined) throw new Error('Block #3 (Пользователь состоит в группе): "User" has no value');

      return user_1.isInGroup(string_0);
    };


    const notOperatorFn_0 = () => {


      return !(UserisInGroupFn_0());
    };

	if (UserisInGroupFn_0() && (issue_0.isChanged("project"))) {
        showMessage("Изменять поле проекта может только администратор!");
        ctx.issue.fields['project'] = ctx.issue.oldValue('project');
        return false;
    }
    console.log(checkUpdatedByFn_0() &&
      notOperatorFn_0());
    try {
      return (
        checkUpdatedByFn_0() &&
      notOperatorFn_0()
      );
    } catch (err) {
      if (err && err.message && err.message.includes('has no value')) {
        logger.error('Failed to execute guard', err);
        return false;
      }
      throw err;
    }

  },
  action: function(ctx) {
    const logger = new Logger(true);


    // --- #1 changeIssueFieldValue ---
    const issue_1 = ctx.issue;
//     logger.log("Mode: set");
//     logger.log(issue_1);
//     logger.log("Current value:", issue_1.fields['Утвержден']);

    const changeIssueFieldValueFn_0 = () => {

      console.log(ctx.Enum_844611.n356)
      issue_1.fields['Утвержден'] = ctx.Enum_844611.n356;
    };

    changeIssueFieldValueFn_0();

  },
  requirements: {
    Enum_844611: {
      name: "Утвержден",
      type: entities.EnumField.fieldType,
      n356: {name: "нет"}
    }
  }
});

function showMessage(what) {
  workflow.message(what, 'Не удалось обновить задачу.');
}

function Logger(useDebug = true) {
  return {
    log: (...args) => useDebug && console.log(...args),
    warn: (...args) => useDebug && console.warn(...args),
    error: (...args) => useDebug && console.error(...args)
  };
}