# music place youtrack workflow
## Ниже описаны скрипты, использующиеся в проекте https://musicplace.youtrack.cloud
### scripts/issue-rules
`Здесь описаны скрипты, относящиеся к изменению задач`
#### auto-rid.js
`Создание Requirements ID внутри создаваемой задачи (Feature)`
#### calc-estimate.js
`Подсчет оценки (своя мера вместо story points)`
#### confirm-req-block.js
`Изменение значения поля "Утвержден" при изменении описания задачи`
### scripts/mp-youtrack-qase-sync
`Здесь описана интеграция с Qase`
#### qase-connection.js
`Подключение к Qase API`
#### youtrack-connection.js
`Подключение к Youtrack API (костыль, нужен, потому что в entities не нашли методы для получения полей)`
#### sync-case.js
`Синхронизация Feature в Youtrack с тест-кейсами в Qase: в Youtrack в задачу в кастомное поле грузим ID тест-кейсов из Qase; вспомогательную информацию о тест-кейсах грузим как таблицу в описание задачи`
### scripts/mp-utils
`Вспомогательные методы`
#### mp-utils.js
`Вспомогательные методы`